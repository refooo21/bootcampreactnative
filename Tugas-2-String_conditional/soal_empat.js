var sentence3 = 'wow JavaScript is so cool'; 

var FirstWord = sentence3.substr(0, 3); 
var secondWord = sentence3.substr(4, 10) ;
var thirdWord = sentence3.substr(15, 2) ;
var fourthWord = sentence3.substr(18,2) ;
var fifthWord = sentence3.substr(21, 4) ;

var firstWordLength = FirstWord.length  ;
var secondWordLength = secondWord.length ;
var thirdWordLength = thirdWord.length ;
var fourthWordLength = fourthWord.length ;
var fifthWordLength = fifthWord.length ;

console.log('First Word: ' + FirstWord + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord + ', with length: ' + fifthWordLength); 